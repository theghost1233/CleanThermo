## Clean CHEMKIN THERMO file.
#### Python3

Creates a copy of the input thermo file but only keeps species that
are in the SPECIES block of the CHEMKIN chem.inp file. By default
the file is written to 'name_clean.ext'.
```bash
Usage:
    clean_therm.py [-i CHEM] [-t THERM] [-o FILE]
    clean_therm.py (-h | --help)
    clean_therm.py --version


Options:
    -h --help  Show this screen.
    --version  Show version.
    -i --input=CHEM  CHEMKIN input file [default: chem.inp].
    -t --thermdat=THERM  Thermo file [default: therm.dat].
    -o FILE --output=FILE  Alternative output file.
```

### Example:
```bash
$ tree
.
├── chem.inp
└── therm.dat

0 directories, 2 files
$ clean_therm
$ tree
.
├── chem.inp
├── therm_clean.dat
└── therm.dat

0 directories, 3 files

```

### Package
```bash
python setup.py sdist
```

### Install
```bash
pip install -e .
```
