#!/usr/bin/env python
"""Clean CHEMKIN THERMO file.

Creates a copy of the input thermo file but only keeps species that
are in the SPECIES block of the CHEMKIN chem.inp file. By default
the file is written to <name_clean.ext>.

Usage:
  clean_therm.py [-i CHEM] [-t THERM] [-o FILE]
  clean_therm.py (-h | --help)
  clean_therm.py --version



Options:
  -h --help  Show this screen.
  --version  Show version.
  -i --input=CHEM  CHEMKIN input file [default: chem.inp].
  -t --thermdat=THERM  Thermo file [default: therm.dat].
  -o FILE --output=FILE  Alternative output file.
"""

import os
import re
from docopt import docopt


def get_thermo_block(species, therm_dat):
    regex = "^" + re.escape(species) + "\s+((.*\n){4})"
    thermo_block = re.search(regex, therm_dat, re.MULTILINE)
    return thermo_block.group(0)


def clean_thermo_seq(species, thermo):
    """Cleans unused species from CHEMKIN thermo file.

    Creates a copy of the input thermo file but only keeps species that are
    in the SPECIES block of the CHEMKIN chem.inp file. By default the file
    is written to <name.ext>_clean.

    Goes sequential trough the entire thermo file and checks if the
    species from the thermo file is also in the set from the mechanism
    file. If so, copy lines until the last line last character equals
    4, since this indicates the end of the species thermo block.
    For completeness also copy over <THERMO> and the global switch
    values (300.00 5000.00 1000.00).

    Args:
        species: List of species to keep in the thermo file.
        thermo: Contend of the CHEMKIN thermo file. Needs to be
        iterable on a line bases, so either comming from open() or
        string with split().

    Returns:
        List of all species thermo blocks which are also in the species
        list. If the thermo file doesn´t contain all species a second
        list is return with all the missing species names.
    """
    out = []
    keep_block = False
    switch_line = re.compile(r"^(\s+\d+\.\d+){3}")
    # significant speed up since re.match is only used till line is found
    switch_line_found = False
    # use set for faster lookup
    species = set(species)
    for line in thermo:
        words = line.strip().split()
        if words:
            first_word = words[0]
            last_char = words[-1][-1]
            if not switch_line_found and first_word.lower() == 'thermo':
                out.append(line)
                continue
            elif not switch_line_found and re.match(switch_line, line):
                out.append(line)
                switch_line_found = True
                continue
            elif first_word in species:
                keep_block = True
                species.remove(first_word)
            if keep_block:
                out.append(line)
            if last_char == '4':
                keep_block = False
    out.append('END\n')

    if species:
        print('Not all species from the CHEMKIN inp file are in the therm.dat')
        print('Missing thermo info for:')
        for s in sorted(species):
            print('\t%s' % s)
        return out, species
    else:
        return out


def get_species_from_chemkin(chem_inp):
    """Extracts species from CHEMKIN input file.

    Get a list of all species in the SPECIES block of a standard
    formated CHEMKIN chemistry file.

    Args:
        chem_inp: Contend of the CHEMKIN input file.

    Returns:
        A list of species. Example:
            ['C7H16',
             'O2',
             'N2',
             'CO2',
             'H2O']
    """
    species = []
    species_block = False
    for line in chem_inp:
        if line.strip().lower() == 'species':
            species_block = True
        elif species_block and line.strip().lower() == 'end':
            return species
        elif species_block:
            species.extend(line.strip().split())


def clean_therm(chem_inp, therm_dat, output_path=''):
    """Main function to run when using python interface

    Args:
        chem_inp:    path to CHEMKIN file
        therm_dat:   path to thermo file
        output_path: Optional path for output file
    """
    with open(chem_inp) as chemkin:
        species = get_species_from_chemkin(chemkin)
    with open(therm_dat) as thermo_file:
        thermo = clean_thermo_seq(species, thermo_file)
    if not output_path:
        output_path = '%s_clean%s' % os.path.splitext(therm_dat)
    with open(output_path, 'w') as output_file:
        for line in thermo:
            output_file.write('%s' % line)


def main():
    """Main function to run when using CLI"""
    arg = docopt(__doc__, version='Clean Thermo 0.1')
    clean_therm(arg['--input'], arg['--thermdat'], arg['--output'])


if __name__ == '__main__':
    main()
