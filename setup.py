#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='cleanThermo',
      version='0.1',
      description='Clean CHEMKIN thermo file',
      author='Casper Meijer',
      author_email='theghost1233@gmail.com',
      url='https://gitlab.com/theghost1233/CleanThermo',
      packages=find_packages(exclude=['tests']),
      license='MIT',
      install_requires=['docopt', 'nose'],
      entry_points={
        'console_scripts': ['clean_therm=clean_therm:main'],
        }
      )
