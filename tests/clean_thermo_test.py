import os
from nose.tools import assert_equal
from cleanThermo import clean_therm


class testCleanThermo():

    def __init__(self):
        self.clean_thermo = ('THERMO\n'
'   300.000  1000.000  5000.000\n'
'H                 120186H   1               G  0300.00   5000.00  1000.00      1\n' # noqa
' 0.02500000E+02 0.00000000E+00 0.00000000E+00 0.00000000E+00 0.00000000E+00    2\n' # noqa
' 0.02547163E+06-0.04601176E+01 0.02500000E+02 0.00000000E+00 0.00000000E+00    3\n' # noqa
' 0.00000000E+00 0.00000000E+00 0.02547163E+06-0.04601176E+01                   4\n' # noqa
'H2                121286H   2               G  0300.00   5000.00  1000.00      1\n' # noqa
' 0.02991423E+02 0.07000644E-02-0.05633829E-06-0.09231578E-10 0.01582752E-13    2\n' # noqa
'-0.08350340E+04-0.01355110E+02 0.03298124E+02 0.08249442E-02-0.08143015E-05    3\n' # noqa
'-0.09475434E-09 0.04134872E-11-0.01012521E+05-0.03294094E+02                   4\n' # noqa
'O                 120186O   1               G  0300.00   5000.00  1000.00      1\n' # noqa
' 0.02542060E+02-0.02755062E-03-0.03102803E-07 0.04551067E-10-0.04368052E-14    2\n' # noqa
' 0.02923080E+06 0.04920308E+02 0.02946429E+02-0.01638166E-01 0.02421032E-04    3\n' # noqa
'-0.01602843E-07 0.03890696E-11 0.02914764E+06 0.02963995E+02                   4\n' # noqa
'O2                121386O   2               G  0300.00   5000.00  1000.00      1\n' # noqa
' 0.03697578E+02 0.06135197E-02-0.01258842E-05 0.01775281E-09-0.01136435E-13    2\n' # noqa
'-0.01233930E+05 0.03189166E+02 0.03212936E+02 0.01127486E-01-0.05756150E-05    3\n' # noqa
' 0.01313877E-07-0.08768554E-11-0.01005249E+05 0.06034738E+02                   4\n') # noqa
        self.species_block = ('ELEMENTS\n'
'O  H  C  N  AR\n' # noqa
'END\n'
'SPECIES\n'
'H2      H       O       O2      OH      H2O     HO2     H2O2    \n'
'C       CH      CH2     CH2(S)  CH3     CH4     CO      CO2     \n'
'HCO     CH2O    CH2OH   CH3O    CH3OH   C2H     C2H2    C2H3    \n'
'C2H4    C2H5    C2H6    HCCO    CH2CO   HCCOH   N       NH      \n'
'NH2     NH3     NNH     NO      NO2     N2O     HNO     CN      \n'
'HCN     H2CN    HCNN    HCNO    HOCN    HNCO    NCO     N2      \n'
'AR      C3H7    C3H8    CH2CHO  CH3CHO\n'
'END     \n')

    def test_get_species_regex(self):
        ref = (
'H2                121286H   2               G  0300.00   5000.00  1000.00      1\n' # noqa
' 0.02991423E+02 0.07000644E-02-0.05633829E-06-0.09231578E-10 0.01582752E-13    2\n' # noqa
'-0.08350340E+04-0.01355110E+02 0.03298124E+02 0.08249442E-02-0.08143015E-05    3\n' # noqa
'-0.09475434E-09 0.04134872E-11-0.01012521E+05-0.03294094E+02                   4\n') # noqa
        out = clean_therm.get_thermo_block('H2', self.clean_thermo)
        assert_equal(out, ref)

    def test_get_species_seq(self):
        ref = ['THERMO',
'   300.000  1000.000  5000.000',
'H2                121286H   2               G  0300.00   5000.00  1000.00      1', # noqa
' 0.02991423E+02 0.07000644E-02-0.05633829E-06-0.09231578E-10 0.01582752E-13    2', # noqa
'-0.08350340E+04-0.01355110E+02 0.03298124E+02 0.08249442E-02-0.08143015E-05    3', # noqa
'-0.09475434E-09 0.04134872E-11-0.01012521E+05-0.03294094E+02                   4', # noqa
'END\n'] # noqa
        out = clean_therm.clean_thermo_seq(['H2'], self.clean_thermo.split('\n')) # noqa
        assert_equal(out, ref)

    def test_get_species_seq_with_comment(self):
        ref = ['THERMO',
'   300.000  1000.000  5000.000',
'H                 120186H   1               G  0300.00   5000.00  1000.00      1', # noqa
' 0.02500000E+02 0.00000000E+00 0.00000000E+00 0.00000000E+00 0.00000000E+00    2', # noqa
' 0.02547163E+06-0.04601176E+01 0.02500000E+02 0.00000000E+00 0.00000000E+00    3', # noqa
' 0.00000000E+00 0.00000000E+00 0.02547163E+06-0.04601176E+01                   4', # noqa
'H2                121286H   2               G  0300.00   5000.00  1000.00      1', # noqa
' 0.02991423E+02 0.07000644E-02-0.05633829E-06-0.09231578E-10 0.01582752E-13    2', # noqa
'-0.08350340E+04-0.01355110E+02 0.03298124E+02 0.08249442E-02-0.08143015E-05    3', # noqa
'-0.09475434E-09 0.04134872E-11-0.01012521E+05-0.03294094E+02                   4', # noqa
'END\n'] # noqa
        inp = self.clean_thermo.split('\n')
        inp.insert(0, '!This is a commented line at the start')
        inp.insert(7, '!This is a comment inbetween species blocks')
        out = clean_therm.clean_thermo_seq(['H2', 'H'], inp)
        assert_equal(out, ref)

    def test_get_species_list(self):
        inp = self.species_block.split('\n')
        ref = ['H2', 'H', 'O', 'O2', 'OH', 'H2O', 'HO2', 'H2O2', 'C', 'CH',
               'CH2', 'CH2(S)', 'CH3', 'CH4', 'CO', 'CO2', 'HCO', 'CH2O',
               'CH2OH', 'CH3O', 'CH3OH', 'C2H', 'C2H2', 'C2H3', 'C2H4', 'C2H5',
               'C2H6', 'HCCO', 'CH2CO', 'HCCOH', 'N', 'NH', 'NH2', 'NH3',
               'NNH', 'NO', 'NO2', 'N2O', 'HNO', 'CN', 'HCN', 'H2CN', 'HCNN',
               'HCNO', 'HOCN', 'HNCO', 'NCO', 'N2', 'AR', 'C3H7', 'C3H8',
               'CH2CHO', 'CH3CHO']
        out = clean_therm.get_species_from_chemkin(inp)
        assert_equal(out, ref)

    def test_clean_therm(self):
        chem_inp = 'tests/chem.inp'
        therm_dat = 'tests/therm.dat'
        therm_dat_ref = 'tests/therm_clean_ref.dat'
        clean_therm.clean_therm(chem_inp, therm_dat)
        assert_equal(open(therm_dat_ref, 'r').read(),
                     open('tests/therm_clean.dat', 'r').read())
        os.remove('tests/therm_clean.dat')
